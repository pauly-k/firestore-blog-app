import * as React from 'react';

function PageLoading (props: any) {
    return (
      <div className="page-loading">
        Loading...
      </div>
    );
}

export default PageLoading