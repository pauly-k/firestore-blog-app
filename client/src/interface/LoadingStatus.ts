export type LoadingStatus =
  | "init"
  | "loading"
  | "loaded"
  | "not-found"
  | "error";

  