export interface BlogSettingData {
  blog_title: string;
  blog_description: string;
  blog_background_cover_url: string;
  loaded: boolean;
}

